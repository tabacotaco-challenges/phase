# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [0.1.4](https://gitlab.com/tabacotaco-challenges/phase/compare/v0.1.3...v0.1.4) (2023-03-07)


### Bug Fixes

* **contexts/selection-context:** add gearing position process when drag the graph ([19bea16](https://gitlab.com/tabacotaco-challenges/phase/commit/19bea16b09a9d7d10fcae54e7254fe5b3ed98a87))

### [0.1.5](https://gitlab.com/tabacotaco-challenges/phase/compare/v0.1.3...v0.1.5) (2023-03-07)


### Bug Fixes

* **contexts/selection-context:** add gearing position process when drag the graph ([19bea16](https://gitlab.com/tabacotaco-challenges/phase/commit/19bea16b09a9d7d10fcae54e7254fe5b3ed98a87))

### [0.1.4](https://gitlab.com/tabacotaco-challenges/phase/compare/v0.1.3...v0.1.4) (2023-03-05)

### [0.1.3](https://gitlab.com/tabacotaco-challenges/phase/compare/v0.1.2...v0.1.3) (2023-03-01)


### Bug Fixes

* **components/auto-align-container:** make sure container will be full height in mobile browser ([f869d7a](https://gitlab.com/tabacotaco-challenges/phase/commit/f869d7a7b4b32f7043adc7375ad47181f2dea692))

### [0.1.2](https://gitlab.com/tabacotaco-challenges/phase/compare/v0.1.1...v0.1.2) (2023-03-01)


### Bug Fixes

* **features/page-canvas:** remove the click event of graphics, and use pointertap ([c86b69f](https://gitlab.com/tabacotaco-challenges/phase/commit/c86b69f18f7375da41ce746379247336a4ac68f1))

### 0.1.1 (2023-03-01)


### Features

* add rwd design ([263e8a7](https://gitlab.com/tabacotaco-challenges/phase/commit/263e8a7f101ab2d5ef5dcadc9b17c9abf54c8a2f))
* **components/action-list:** add new component(ActionList) ([34f7c01](https://gitlab.com/tabacotaco-challenges/phase/commit/34f7c0116c257a5bd24dbb85d34f2638d2065277))
* **components/auto-align-container:** add new component ([eb17579](https://gitlab.com/tabacotaco-challenges/phase/commit/eb175794a66b8335fec7f8ec6e6b5ffc12ff7b77))
* **components/collapsed-button:** add new component ([10c701e](https://gitlab.com/tabacotaco-challenges/phase/commit/10c701ecacf0b78d8a6aed40231676c1f9b7747b))
* **components/color-picker:** add new component ([a810096](https://gitlab.com/tabacotaco-challenges/phase/commit/a810096700d4e28e780d1e4b48ee82adf6e1a7e8))
* **components/no-data-list-item:** add new component(NoDataListItem) ([54fefc3](https://gitlab.com/tabacotaco-challenges/phase/commit/54fefc39a0b379e9a1e9f79f922bae941248b61f))
* **components/node-collapse:** add breadcrumbs dropdown ([f57ec78](https://gitlab.com/tabacotaco-challenges/phase/commit/f57ec7890a23e922fb091824f2c0584ae0343c59))
* **components/node-collapse:** add new components ([005cbb7](https://gitlab.com/tabacotaco-challenges/phase/commit/005cbb794d05c133b9249e5e939ff42b052e39d0))
* **components/node-item:** add edit mode ([21ff906](https://gitlab.com/tabacotaco-challenges/phase/commit/21ff906f09b459c14c98e60db5b84ae78b8e9bb1))
* **components/nodeitem:** add new component(NodeItem) ([0568c18](https://gitlab.com/tabacotaco-challenges/phase/commit/0568c18a23e3b0263b6d62aa049a34328192ff9e))
* **components/number-field:** add new component ([cf5d0da](https://gitlab.com/tabacotaco-challenges/phase/commit/cf5d0da52c099b0e839e61b8aae6dfcf914be877))
* **components/persistent-drawer:** add new component ([b640890](https://gitlab.com/tabacotaco-challenges/phase/commit/b6408907fb4119e792429d93591c02c9cfde7d60))
* **components:** add 2 new number field components ([7dfdff7](https://gitlab.com/tabacotaco-challenges/phase/commit/7dfdff75e7a6fafd2e790d8de9d043740bd6314e))
* **contexts/actived-context:** add new context ([0fdd1f4](https://gitlab.com/tabacotaco-challenges/phase/commit/0fdd1f4226e3849e1f450a9920aa2fde5909d6b3))
* **contexts/selection-context:** add new context to manage node selection ([5d4f234](https://gitlab.com/tabacotaco-challenges/phase/commit/5d4f2347f552dd3b2420c9c867ddde684ee7bbff))
* **contexts/selection-context:** add onSelect event ([1556adb](https://gitlab.com/tabacotaco-challenges/phase/commit/1556adbb1d939c54f92d4e758a2113ab918c1691))
* **features/app-main:** add .test.tsx ([2669dff](https://gitlab.com/tabacotaco-challenges/phase/commit/2669dffba9137b1c90503e213f07576b348ff3fc))
* **features/app-main:** add app-main component ([040582c](https://gitlab.com/tabacotaco-challenges/phase/commit/040582c12754b922fddae75b53f6778822b16be1))
* **features/element-editor:** add new feature component ([267d47f](https://gitlab.com/tabacotaco-challenges/phase/commit/267d47f14f40b85657493012b45a5f441df20475))
* **features/element-list, features/page-list:** add edit event ([baeb9ba](https://gitlab.com/tabacotaco-challenges/phase/commit/baeb9baf0fe7ddb9288b8d67dace28da6cc89b88))
* **features/element-list:** add new feature component(ElementList) ([52fc961](https://gitlab.com/tabacotaco-challenges/phase/commit/52fc961c699d0d0b7fa991147fa6b1ae6b771a30))
* **features/page-canvas:** add new feature component ([f1ce1df](https://gitlab.com/tabacotaco-challenges/phase/commit/f1ce1df2f5614fd63a24bfc102a07e6fe8c8aa47))
* **features/page-canvas:** add selection highlight ([b47b47f](https://gitlab.com/tabacotaco-challenges/phase/commit/b47b47f35832f1cea2271cbb225e4194cc8bc748))
* **features/page-list:** add new feature component(PageList) ([2e3bc9e](https://gitlab.com/tabacotaco-challenges/phase/commit/2e3bc9eb304f5a26ecb25be946fe89e4eae7841c))
* **features:** add unselect in PageList/ElementList ([128066c](https://gitlab.com/tabacotaco-challenges/phase/commit/128066c51ec717d0ef699c91ba805970c604fd0a))
* **hooks/use-differences:** add onFinally to get all elements ([85a311b](https://gitlab.com/tabacotaco-challenges/phase/commit/85a311b840e48ab7a0b514789d43ed154d69b2a2))
* **hooks/use-element-store:** add new features ([f443358](https://gitlab.com/tabacotaco-challenges/phase/commit/f443358991af1b1a71b1d844ce1acd7af1fe53eb))
* **hooks/use-element-store:** add new hooks to manage data ([90b91fe](https://gitlab.com/tabacotaco-challenges/phase/commit/90b91fe2861ffcd5b1b16e17dd435aff0f01094f))
* **hooks/use-graphics:** add new hooks ([60b98b7](https://gitlab.com/tabacotaco-challenges/phase/commit/60b98b73161183c8d49a83030c4236c17889c4a4))
* **hooks/use-width:** add new custom hook ([c94baa1](https://gitlab.com/tabacotaco-challenges/phase/commit/c94baa18f417d4ee77d67da5f29f4f650e201729))
* **pages/_app:** include app-main ([b6d47e4](https://gitlab.com/tabacotaco-challenges/phase/commit/b6d47e430ee5aafb85c392fe01c2ad51977a277e))
* **pages/index:** revise layout ([7204d21](https://gitlab.com/tabacotaco-challenges/phase/commit/7204d2176f041f1b3b4b53b7621208628c8d9d96))
* **styles/canvas:** add new styles-component ([ee9b398](https://gitlab.com/tabacotaco-challenges/phase/commit/ee9b39883bcf437b91c4586b202102f45dcdcc5b))
* **styles/mui-button:** add button styles ([e9badc9](https://gitlab.com/tabacotaco-challenges/phase/commit/e9badc9c2503d34521fcf675899e1e5af16e7ca1))
* **styles/mui-drawer:** add new styles components ([ee900e6](https://gitlab.com/tabacotaco-challenges/phase/commit/ee900e6946ad0e660aef52576c84c7859ad6bc37))
* **styles/mui-fab:** add new styles component ([3933d11](https://gitlab.com/tabacotaco-challenges/phase/commit/3933d115b0617bcd05366fc87c17687bc0cfa20c))
* **styles/mui-list-item-button:** add new styles-component ([9a450d5](https://gitlab.com/tabacotaco-challenges/phase/commit/9a450d5781a5d9b62df843e1d971649e2bb1645d))
* **styles:** add new styles component ([8683cc3](https://gitlab.com/tabacotaco-challenges/phase/commit/8683cc364300a8f937310ac6c4c9f7c1370f0d8b))
* **themes:** add dark theme ([8e2effd](https://gitlab.com/tabacotaco-challenges/phase/commit/8e2effd6e55145360e43a088f2475b2259bce919))


### Bug Fixes

* ***.styles.ts:** re-move all the *.styles.ts to /styles ([8576d92](https://gitlab.com/tabacotaco-challenges/phase/commit/8576d924c61fad5c015485800891f35015a61e5c))
* change the initail color of the element ([ffbb457](https://gitlab.com/tabacotaco-challenges/phase/commit/ffbb4575cae62f8f56c1b6d036bd1a15acb99a40))
* change to install throttle-debounce ([a29ccc7](https://gitlab.com/tabacotaco-challenges/phase/commit/a29ccc740c0b686c891a8462b9ce01e0bcabd002))
* **components/action-list:** remove .test.tsx ([1fad0f8](https://gitlab.com/tabacotaco-challenges/phase/commit/1fad0f88995fd190a3cd6dcd7c31e9651a8b7dc7))
* **components/auto-align-container:** remove console.log ([50a3930](https://gitlab.com/tabacotaco-challenges/phase/commit/50a393038b0816022df01ad6a0b10199351b2213))
* **components/color-picker, components/number-field, components/number-slider-field:** re-tidy ([2eee105](https://gitlab.com/tabacotaco-challenges/phase/commit/2eee10592f4d0b094dea34d2a52cbe735006dd89))
* **components/color-picker,components/number-field, components/number-slider-field:** fixed type ([454b7ab](https://gitlab.com/tabacotaco-challenges/phase/commit/454b7ab9b444bea7e49a0c55c444270a69bea6c7))
* **components/color-picker:** reset debounce time ([6fc0e83](https://gitlab.com/tabacotaco-challenges/phase/commit/6fc0e831e6c8e312e151bec5b3a1dce254e0a917))
* **components/no-data-list-item:** surport more props of ListItem ([439dfa3](https://gitlab.com/tabacotaco-challenges/phase/commit/439dfa3fe8ea05bc4b704d6eac0f4603b6f816fe))
* **components/node-collapse:** move to components from features ([8519fa6](https://gitlab.com/tabacotaco-challenges/phase/commit/8519fa61543e0729fa5bc5fd4ad00dde962bd856))
* **components/node-collapse:** reset breadcrumbs styles ([9223362](https://gitlab.com/tabacotaco-challenges/phase/commit/922336254d7982595e27a276bb31fdf0b2cf1674))
* **components/node-item:** change the prop-types ([15c53ef](https://gitlab.com/tabacotaco-challenges/phase/commit/15c53ef7ace883ccd987627c37a15175536e626b))
* **components/node-item:** fixed the error of onNodeSelect ([e47b5dd](https://gitlab.com/tabacotaco-challenges/phase/commit/e47b5ddcd1ed3f479f8885f29af7ce3e341a42c3))
* **components:** fixed the onChange input types ([4335f2f](https://gitlab.com/tabacotaco-challenges/phase/commit/4335f2ff98794363eede4b3bc70d42bb0a592c50))
* **contexts/actived-context:** fixed the anti pattern ([229db7a](https://gitlab.com/tabacotaco-challenges/phase/commit/229db7a95eee445282af4ce663f3dfa94d960220))
* **contexts/actived-context:** make input node could be empty in useContext, and exported new types ([6b3034c](https://gitlab.com/tabacotaco-challenges/phase/commit/6b3034cff5ff693eba46db0dbb0b5e327f9dab96))
* **contexts/actived-context:** re-tidy the design pattern ([6d35e6f](https://gitlab.com/tabacotaco-challenges/phase/commit/6d35e6f315222c13393d76eefeee96e681333b7c))
* **contexts/actived-context:** revise the return value of useActivedContext ([e6ad64c](https://gitlab.com/tabacotaco-challenges/phase/commit/e6ad64c4f37d7e58ca4b87e44de106e062c5cf61))
* **contexts/selection-context:** check onSelect is a function before call it ([e8e8ebd](https://gitlab.com/tabacotaco-challenges/phase/commit/e8e8ebdb0a194b5c862b106c4460e4ec14f1580c))
* **contexts/selection-context:** export type of context value ([b1efc24](https://gitlab.com/tabacotaco-challenges/phase/commit/b1efc240d9ce97435b01159c07efe6b26bc640b9))
* **features/app-main:** remove testing code ([48e6916](https://gitlab.com/tabacotaco-challenges/phase/commit/48e69166de546df0e37886fff0e87f2fb088d2da))
* **features/element-editor:** remove unused async/await ([ed64bc0](https://gitlab.com/tabacotaco-challenges/phase/commit/ed64bc0abe5a783003eb9f4e432e17511cb54b88))
* **features/element-editor:** revise fixed ([febe7c3](https://gitlab.com/tabacotaco-challenges/phase/commit/febe7c311565a44da6e69e600c92cdf359a01aa5))
* **features/element-editor:** revise fixed ([e2f5aa3](https://gitlab.com/tabacotaco-challenges/phase/commit/e2f5aa315fd2b7996b47bb5a2f41ab2582fb0f46))
* **features/element-editor:** revise update with ColorPicker ([1cec8d8](https://gitlab.com/tabacotaco-challenges/phase/commit/1cec8d888f4dbe0a655e710cb836ad2e883d04f5))
* **features/element-editor:** revise ux ([6015546](https://gitlab.com/tabacotaco-challenges/phase/commit/6015546baac04009473584f8783a5c43f842e927))
* **features/element-list, features/page-list:** include and use NodeItem ([9a1e9da](https://gitlab.com/tabacotaco-challenges/phase/commit/9a1e9da5d57056e4f31e635224f31d6c19012b2e))
* **features/element-list:** add description field ([e765a81](https://gitlab.com/tabacotaco-challenges/phase/commit/e765a81161f6053cc1d59000bc9207366ed301dd))
* **features/node-item:** re-tidy with the modified of ActivedContext ([7885886](https://gitlab.com/tabacotaco-challenges/phase/commit/788588683e4c6ff8dd0a34af7c591c4def823cbe))
* **features/node-item:** remove unused import ([b8c1bfd](https://gitlab.com/tabacotaco-challenges/phase/commit/b8c1bfdc9d6e649caf01fb85456ee78c36115bd5))
* **features/page-canvas:** change the event name as use pointer event ([5b9882b](https://gitlab.com/tabacotaco-challenges/phase/commit/5b9882b626ad2484c6558fa3bd637ffab7443a58))
* **features/page-canvas:** remove duplication hook ([49639fa](https://gitlab.com/tabacotaco-challenges/phase/commit/49639faa6c0efdf3b1604072ef652ae371baa110))
* **features/page-canvas:** remove invalid export type ([63b8fde](https://gitlab.com/tabacotaco-challenges/phase/commit/63b8fde5cbf7d27a025f23c0707e963a7a7ace61))
* **features/page-canvas:** remove unused import ([407b588](https://gitlab.com/tabacotaco-challenges/phase/commit/407b588f70f4c94075ac1cba3a767bd450145e56))
* **features/page-canvas:** rename hook ([d6ae30f](https://gitlab.com/tabacotaco-challenges/phase/commit/d6ae30f2729373e2bd7852286a75747f20b0449a))
* **features:** fixed the selected uid still exists after remove item ([47c6ce5](https://gitlab.com/tabacotaco-challenges/phase/commit/47c6ce5cb3bc46f3b8a3a1bde1afc5305f60cb1b))
* fixed eslint issues ([cc41d9a](https://gitlab.com/tabacotaco-challenges/phase/commit/cc41d9afb18e85559d21f8ce073f31ca52293dee))
* fixed the error of selection graphics will not grear with updating postiion by editor ([2cd0749](https://gitlab.com/tabacotaco-challenges/phase/commit/2cd0749b37c4d52aca0fff9d373734d1c03c9c13))
* **hooks/use-differences:** rename useElements to useDifferences ([a0b1f97](https://gitlab.com/tabacotaco-challenges/phase/commit/a0b1f97614432539b0b4b8b97f8805a92789eccd))
* **hooks/use-element-store, features/page-canvas:** element data add graph to save the Graphics ([afdee91](https://gitlab.com/tabacotaco-challenges/phase/commit/afdee915489e1f02da16d07c9a2413115a2b362a))
* **hooks/use-element-store:** add draw function to utils ([2b921b5](https://gitlab.com/tabacotaco-challenges/phase/commit/2b921b5f4af5d3b37a170ffb8ffb3ffb0da62c94))
* **hooks/use-element-store:** add new type for getElementPaths ([ab149b2](https://gitlab.com/tabacotaco-challenges/phase/commit/ab149b2ad2acedeb1778ac17e4862cdf506a0010))
* **hooks/use-element-store:** add return edited-data in edit method ([6ce917a](https://gitlab.com/tabacotaco-challenges/phase/commit/6ce917ad838b558c158dc95aab4192b720025e5b))
* **hooks/use-element-store:** add will return uid of new data ([f162b5b](https://gitlab.com/tabacotaco-challenges/phase/commit/f162b5b0d97583f8e88ad17847227dc7f731a344))
* **hooks/use-element-store:** change the export types ([99226b8](https://gitlab.com/tabacotaco-challenges/phase/commit/99226b8fa0a872a973f146c293cb4873fa814e5c))
* **hooks/use-element-store:** export new type ([cb44103](https://gitlab.com/tabacotaco-challenges/phase/commit/cb441037bc4259a343ce188e0d7c50326f7872fc))
* **hooks/use-element-store:** fixed the add method type ([dfadb96](https://gitlab.com/tabacotaco-challenges/phase/commit/dfadb962c0df69de78973218551718a3cde2f83b))
* **hooks/use-element-store:** fixed the bug which lose to register child uid in parent ([fa3e73d](https://gitlab.com/tabacotaco-challenges/phase/commit/fa3e73d1cbb1bd4ba9be861848d920021bed1dbe))
* **hooks/use-element-store:** fixed the bugs of recursion in getElements ([ec39a99](https://gitlab.com/tabacotaco-challenges/phase/commit/ec39a993c5a856cf26edf4f0a1a7cce5778b61b5))
* **hooks/use-element-store:** fixed the edit method types ([214af54](https://gitlab.com/tabacotaco-challenges/phase/commit/214af54f12899590c7e8f5cee1710b569989e30e))
* **hooks/use-element-store:** fixed the PageData types ([1854551](https://gitlab.com/tabacotaco-challenges/phase/commit/1854551b287c2028ea46182f1604d0d9e401afa7))
* **hooks/use-element-store:** fixed the types of state ([81e92b5](https://gitlab.com/tabacotaco-challenges/phase/commit/81e92b5609b6db35a2000a73452c7322b27d2a85))
* **hooks/use-element-store:** tidy code ([486774c](https://gitlab.com/tabacotaco-challenges/phase/commit/486774c16ba65abf46e68fa121f0797aa79ed87c))
* **hooks/use-element-store:** transform add/remove/edit method to return promise ([b736aa8](https://gitlab.com/tabacotaco-challenges/phase/commit/b736aa86a144815359b1aa0aa84898e98a70b0e8))
* **hooks/use-element-store:** update I/O params of add method ([77300a0](https://gitlab.com/tabacotaco-challenges/phase/commit/77300a0943ff7a246021046632d2c18e17663043))
* **hooks/use-elements:** remove useElements ([cca67aa](https://gitlab.com/tabacotaco-challenges/phase/commit/cca67aa391784c40f5c45f250847b6a06b911211))
* include SelectionContext ([836202e](https://gitlab.com/tabacotaco-challenges/phase/commit/836202e73eca4b24b82dfde3c8b528bac59b0e60))
* **pages/index:** re-tidy rwd ([46b9f34](https://gitlab.com/tabacotaco-challenges/phase/commit/46b9f34ef92d7e211d431e6f4a5e13b83a07edd1))
* **pages/index:** reset grid styles ([af38a6b](https://gitlab.com/tabacotaco-challenges/phase/commit/af38a6bef89b016257cb5e81ec8ce5e87323d904))
* **pages/index:** revise fixed ([360341b](https://gitlab.com/tabacotaco-challenges/phase/commit/360341bac39517f64092aab781e052c686321c73))
* re-style components ([9e1d23d](https://gitlab.com/tabacotaco-challenges/phase/commit/9e1d23dcaee6632af4ebac24c35ba539d0c7b9f7))
* re-tidy graph render rules ([7c9f5db](https://gitlab.com/tabacotaco-challenges/phase/commit/7c9f5db3bc610ff3e64ef38a405533e9a60ffb7d))
* reset collapse styles ([4b54cfd](https://gitlab.com/tabacotaco-challenges/phase/commit/4b54cfd105e9fe810a0a2a4bad8bf58379f01749))
* reset styles ([29a6a99](https://gitlab.com/tabacotaco-challenges/phase/commit/29a6a9991b23738088d581242fbe6ed49e95db29))
* reset styles ([4697875](https://gitlab.com/tabacotaco-challenges/phase/commit/4697875aa84ec83603f61ca6de87d9879863e1af))
* revise with ActivedContext/useElementStore ([2a5921b](https://gitlab.com/tabacotaco-challenges/phase/commit/2a5921bfc90ed7e4e2d9340aadcdbc49b5822c4b))
* revise/fixed ([6f5f7b2](https://gitlab.com/tabacotaco-challenges/phase/commit/6f5f7b23b4af671a27eb54fa6835bc8aa58961a6))
* **styles/mui-button:** re-style/re-name component ([b11114f](https://gitlab.com/tabacotaco-challenges/phase/commit/b11114fba6f76192826a56d92c2a3f98de00d679))
* **styles/mui-container:** reset styles ([2a4f2f4](https://gitlab.com/tabacotaco-challenges/phase/commit/2a4f2f429aeb29d5bfbf1b193a5c4d7f8b1747e6))
* **styles/mui-list-item-text:** re-tidy props ([35c8dda](https://gitlab.com/tabacotaco-challenges/phase/commit/35c8ddaa48ea4402ca794b41cd2cd471596b1a99))
* **styles/mui-toolbar:** add styles-component(MuiToolbar) ([09af82a](https://gitlab.com/tabacotaco-challenges/phase/commit/09af82ae3a1bfeddee1cd0d2ea86d57cad8e9751))
* **styles:** rename styles-component ([42534a9](https://gitlab.com/tabacotaco-challenges/phase/commit/42534a90483e3fe2f57b857d0a17ab10c07d7682))
* **themes/dark:** reset primary color ([d210ec2](https://gitlab.com/tabacotaco-challenges/phase/commit/d210ec2b990a3fb849425406f2a27ba42a28bf3b))
* **themes/dark:** update the primary color ([acac350](https://gitlab.com/tabacotaco-challenges/phase/commit/acac350187d58f01139e99a1e6aa0532c7b970ea))
* update component structure ([68894db](https://gitlab.com/tabacotaco-challenges/phase/commit/68894db14f166e19b25a436f7fdaf67fd29255a4))
* update style ([f358f81](https://gitlab.com/tabacotaco-challenges/phase/commit/f358f8125254dea3ed0e3710fd13d1fa51775236))
