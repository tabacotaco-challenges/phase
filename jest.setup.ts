// Optional: configure or set up a testing framework before each test.
// If you delete this file, remove `setupFilesAfterEnv` from `jest.config.js`

// Used for __tests__/testing-library.js
// Learn more: https://github.com/testing-library/jest-dom
import '@testing-library/jest-dom';
import 'canvas';
import 'jest-webgl-canvas-mock';

Object.defineProperty(window.MouseEvent.prototype, 'isPrimary', {
  value: false,
  writable: true,
});

Object.defineProperties(window, {
  PointerEvent: {
    value: window.MouseEvent,
  },
  TouchEvent: {
    value: window.MouseEvent,
  },
});
