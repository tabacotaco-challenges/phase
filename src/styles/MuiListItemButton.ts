import ListItemButton from '@mui/material/ListItemButton';
import { withStyles } from 'tss-react/mui';

export const ArcListItemButton = withStyles(
  ListItemButton,
  (theme) => ({
    root: {
      borderRadius: `${theme.spacing(2)} / 50%`,
    },
  }),
  { name: 'ArcListItemButton' }
);
