import Collapse from '@mui/material/Collapse';
import { withStyles } from 'tss-react/mui';

export const SubCollapse = withStyles(
  Collapse,
  (theme) => ({
    root: {
      borderLeft: `1px solid ${theme.palette.divider}`,
      borderBottom: `1px solid ${theme.palette.divider}`,
      borderBottomLeftRadius: theme.spacing(1),
      marginLeft: theme.spacing(4.5),
      marginBottom: theme.spacing(2),
    },
  }),
  { name: 'SubCollapse' }
);
