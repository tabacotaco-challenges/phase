import Container from '@mui/material/Container';
import { withStyles } from 'tss-react/mui';

export const MainContainer = withStyles(
  Container,
  (theme) => ({
    root: {
      position: 'relative',
      display: 'flex',
      flexDirection: 'column',
      paddingTop: theme.spacing(1),
      paddingBottom: theme.spacing(1),
      overflow: 'hidden auto',
    },
  }),
  { name: 'MainContainer' }
);
