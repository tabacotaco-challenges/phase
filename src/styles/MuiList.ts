import List from '@mui/material/List';
import { withStyles } from 'tss-react/mui';

export const GapList = withStyles(
  List,
  (theme) => ({
    root: {
      marginBottom: theme.spacing(1),

      '& > * + *': {
        marginTop: `${theme.spacing(0.5)} !important`,
      },
    },
  }),
  { name: 'GapList' }
);
