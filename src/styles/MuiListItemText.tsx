import ListItemText, { ListItemTextProps } from '@mui/material/ListItemText';
import { withStyles } from 'tss-react/mui';

export const ActionableListItemText = withStyles(
  ({
    withAction = false,
    ...props
  }: ListItemTextProps & { withAction: boolean }) => (
    <ListItemText {...props} />
  ),
  (theme, { withAction }) => ({
    root: {
      paddingRight: theme.spacing(withAction ? 5 : 0),
    },
  }),
  { name: 'ActionableListItemText' }
);
