import Fab from '@mui/material/Fab';
import { withStyles } from 'tss-react/mui';

export const FixedFab = withStyles(
  Fab,
  (theme) => ({
    root: {
      position: 'fixed',
      bottom: theme.spacing(2),
      right: theme.spacing(2),
      zIndex: theme.zIndex.fab,
    },
  }),
  { name: 'FixedFab' }
);
