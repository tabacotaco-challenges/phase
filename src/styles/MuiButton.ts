import Button from '@mui/material/Button';
import { withStyles } from 'tss-react/mui';

export const TextButton = withStyles(
  Button,
  () => ({
    root: {
      textTransform: 'capitalize',
      padding: 0,
      minWidth: 'auto',
    },
  }),
  { name: 'TextButton' }
);
