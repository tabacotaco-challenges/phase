import { styled } from '@mui/material/styles';

export const FilledCanvas = styled('canvas', { name: 'FilledCanvas' })(
  ({ theme }) => ({
    borderRadius: theme.shape.borderRadius,
    width: '100%',
    height: '100%',
  })
);
