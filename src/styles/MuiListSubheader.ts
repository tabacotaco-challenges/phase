import ListSubheader from '@mui/material/ListSubheader';
import { withStyles } from 'tss-react/mui';

export const ArcListSubheader = withStyles(
  ListSubheader,
  (theme) => ({
    root: {
      borderRadius: `${theme.spacing(2)} / 50%`,
    },
  }),
  { name: 'ArcListSubheader' }
);
