import Toolbar from '@mui/material/Toolbar';
import { withStyles } from 'tss-react/mui';

export const SpaceBetweenToolbar = withStyles(
  Toolbar,
  (theme) => ({
    root: {
      justifyContent: 'space-between',
    },
  }),
  { name: 'SpaceBetweenToolbar' }
);
