import Drawer, { DrawerProps } from '@mui/material/Drawer';
import { withStyles } from 'tss-react/mui';

export const AbsoluteDrawer = withStyles(
  ({ maxWidth, ...props }: DrawerProps & { maxWidth: string | number }) => (
    <Drawer {...props} variant="persistent" />
  ),
  (theme, { maxWidth }) => ({
    root: {
      position: 'unset !important' as 'unset',
      width: maxWidth,
      zIndex: theme.zIndex.mobileStepper,
    },
    paper: {
      position: 'absolute !important' as 'absolute',
      width: maxWidth,
    },
  }),
  { name: 'AbsoluteDrawer' }
);
