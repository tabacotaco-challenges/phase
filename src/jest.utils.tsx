import CssBaseline from '@mui/material/CssBaseline';
import NoSsr from '@mui/material/NoSsr';
import { ReactElement } from 'react';
import { ThemeProvider } from '@mui/material/styles';
import { render, RenderOptions } from '@testing-library/react';

import * as themes from '~/themes';
import { MainContainer } from '~/styles';

const Wrapper = ({ children }: { children: React.ReactNode }) => (
  <NoSsr>
    <ThemeProvider theme={themes.dark}>
      <CssBaseline />

      <MainContainer maxWidth="lg" className="app" component="main">
        {children}
      </MainContainer>
    </ThemeProvider>
  </NoSsr>
);

const customRender = (ui: ReactElement, options?: Omit<RenderOptions, 'wrapper'>) =>
  render(ui, {
    wrapper: Wrapper,
    ...options,
  });

export * from '@testing-library/react';
export { customRender as render };
