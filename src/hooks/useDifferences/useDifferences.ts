import { useEffect, useRef } from 'react';

import * as Types from './useDifferences.types';
import { ElementData, useElementStore } from '../useElementStore';

const useDifferences: Types.DifferencesHook = (
  uid,
  { onAdd, onRemove, onFinally, includeSelf = false },
) => {
  const prevRef = useRef<ElementData[]>([]);
  const data = useElementStore(({ data }) => data);

  const elements = useElementStore(
    ({ getElements }) => {
      const result = getElements(uid, true);

      return (
        !includeSelf || !data.has(uid) ? result : [data.get(uid), ...result]
      ) as ElementData[];
    },

    //* Make sure "list" are not equals
    (e1, e2) => {
      const set2 = new Set(e2.map(({ uid }) => uid));

      return e1.every(({ uid }) => set2.delete(uid)) && set2.size === 0;
    },
  );

  useEffect(() => {
    const removeds = prevRef.current.reduce<Map<string, ElementData>>(
      (result, prevEl) => result.set(prevEl.uid, prevEl),
      new Map(),
    );

    elements.forEach((element) => {
      removeds.delete(element.uid);
      onAdd(element);
    });

    Array.from(removeds.values()).forEach((element) => onRemove(element));
    prevRef.current = elements;
    onFinally?.(elements);
  }, [elements, onAdd, onRemove, onFinally]);

  return elements;
};

export default useDifferences;
