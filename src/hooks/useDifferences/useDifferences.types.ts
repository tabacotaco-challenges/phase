import { ElementData } from '../useElementStore';

export type DifferencesHook = (
  superior: string,
  options: {
    includeSelf?: boolean;
    onAdd: (element: ElementData) => void;
    onRemove: (element: ElementData) => void;
    onFinally?: (elements: ElementData[]) => void;
  }
) => ElementData[];
