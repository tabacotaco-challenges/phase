import { Breakpoint } from '@mui/system';

export type WidthHook = () => Breakpoint;
