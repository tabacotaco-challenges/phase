import { Graphics } from 'pixi.js';
import * as Types from './useElementStore.types';

export const draw: Types.DrawFn = (element) => {
  const { graph, positionX, positionY, opacity, color } = element;
  const color16 = Number.parseInt(color.substring(1), 16);

  graph
    .clear()
    .setTransform(positionX, positionY)
    .beginFill(color16, 1)
    .drawRect(0, 0, 100, 100)
    .endFill();

  graph.alpha = opacity;
  graph.fill.color = color16;

  return element;
};

export const initPageData: Types.InitPageDataFn = (uid) => ({
  type: 'page',
  uid,
  description: '',
});

export const initElementData: Types.InitElementDataFn = (uid, { superior, color }) => {
  const graph = new Graphics();

  graph.name = uid;
  graph.interactive = true;
  graph.cursor = 'pointer';

  return draw({
    type: 'element',
    uid,
    superior,
    description: '',
    positionX: 0,
    positionY: 0,
    opacity: 1,
    color,
    graph,
  });
};

export const removeRecursion: Types.RemoveRecursionFn = (data, children) =>
  children?.forEach((uid) => {
    if (data.has(uid)) {
      const target = data.get(uid);

      removeRecursion(data, target?.children);
      target?.graph.destroy(true);
      data.delete(uid);
    }
  });
