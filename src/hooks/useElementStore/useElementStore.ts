import { create } from 'zustand';
import { generate as uuid } from 'shortid';

import * as Types from './useElementStore.types';
import * as utils from './useElementStore.utils';

const useElementStore = create<Types.ElementStoreState>((set, get) => ({
  data: new Map<string, Types.ElementStoreData>(),

  //* Get Data
  getElements: (superior, recursion = false) => {
    const { data, getElements } = get();
    const list = Array.from(data.get(superior)?.children || []);

    return list.reduce<Types.ElementData[]>((result, uid) => {
      if (data.has(uid)) {
        result.push(
          data.get(uid) as Types.ElementData,
          ...(!recursion ? [] : getElements(uid, true)),
        );
      }

      return result;
    }, []);
  },

  //* Modify Data
  add: async (options) => {
    const { data } = get();
    const uid = uuid();

    const newData =
      options === 'page' ? utils.initPageData(uid) : utils.initElementData(uid, options);

    if (data.has(newData.superior)) {
      const parent = data.get(newData.superior as string);
      const { children = new Set() } = parent;

      parent.children = children.add(uid);
    }

    data.set(uid, newData);
    set({ data });

    return uid;
  },
  edit: async (uid, editData) => {
    const { data } = get();

    if (data.has(uid)) {
      const currData = { ...data.get(uid), ...editData };

      data.set(uid, currData);
      set({ data });

      if (currData.type === 'element') {
        utils.draw(currData);
      }
    }
  },
  remove: async (uid) => {
    const { data } = get();

    if (data.has(uid)) {
      const { superior, children, graph } = data.get(uid);
      const parent = data.get(superior as string);

      parent?.children?.delete(uid);
      graph?.destroy(true);
      data.delete(uid);
      utils.removeRecursion(data, children);
      set({ data });
    }
  },
}));

export default useElementStore;
