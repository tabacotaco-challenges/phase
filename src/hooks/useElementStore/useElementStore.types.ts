import { Graphics } from 'pixi.js';

export type NodeType = 'page' | 'element';

interface BaseData<T extends NodeType> {
  type: T;
  uid: string;
  description: string;
  superior?: string;
  children?: Set<string>;
  graph?: Graphics;
}

export interface PageData extends BaseData<'page'> {}

export interface ElementData extends BaseData<'element'> {
  positionX: number;
  positionY: number;
  opacity: number;
  color: string;
}

type DefaultElementData = Partial<Omit<ElementData, 'uid' | 'graphy'>> &
  Pick<ElementData, 'superior' | 'color'>;

export type ElementStoreData = PageData | ElementData;
export type InitPageDataFn = (uid: string) => PageData;
export type DrawFn = (element: ElementData) => ElementData;

export type InitElementDataFn = (
  uid: string,
  defaultElement: Partial<Pick<ElementData, 'superior' | 'color'>>,
) => ElementData;

export type RemoveRecursionFn = (
  data: Map<string, ElementStoreData>,
  children?: Set<string>,
) => void;

export interface ElementStoreState {
  data: Map<string, ElementStoreData>;

  getElements: (superior: string, recursion?: boolean) => ElementData[];

  add: (options: 'page' | DefaultElementData) => Promise<string>;
  remove: (uid: string) => Promise<void>;

  edit: (
    uid: string,
    editData:
      | Partial<Omit<PageData, 'type' | 'superior' | 'children'>>
      | Partial<Omit<ElementData, 'type' | 'superior' | 'children'>>,
  ) => void;
}
