import * as Types from './PageCanvas.types';
import { FilledCanvas } from '~/styles';
import { usePixiApp, useDnd } from './PageCanvas.hooks';

export default function PageCanvas({ onGraphicsChange }: Types.PageCanvasProps) {
  const id = usePixiApp();
  const onDragging = useDnd(onGraphicsChange);

  return (
    <FilledCanvas
      data-testid="canvas"
      id={id}
      onPointerMove={onDragging}
      onPointerUp={(e) => onDragging(e, true)}
    />
  );
}
