import { Application, Sprite } from 'pixi.js';
import { MouseEvent, useImperativeHandle, useRef } from 'react';
import { generate as uuid } from 'shortid';
import { useTheme } from '@mui/material/styles';

import * as Types from './PageCanvas.types';
import { ElementData, useDifferences, useElementStore } from '~/hooks';
import { useSelectionContext } from '~/contexts';

export const usePixiApp: Types.PixiAppHook = () => {
  const theme = useTheme();
  const { pixiRef, onNodeSelect } = useSelectionContext();
  const { current: id } = useRef(uuid());

  useImperativeHandle(
    pixiRef,
    () => {
      const view = document.getElementById(id) as HTMLCanvasElement;
      const background = new Sprite();

      const app = new Application({
        view,
        background: theme.palette.background.paper,
        resizeTo: view.parentElement,
      });

      background.width = app.screen.width;
      background.height = app.screen.height;
      background.interactive = true;
      background.on('click', () => onNodeSelect('element', null));

      app.stage.addChildAt(background, 0);
      pixiRef.current?.destroy();

      return app;
    },
    [id, pixiRef, theme, onNodeSelect],
  );

  return id;
};

export const useDnd: Types.DndHook = (onGraphicsChange) => {
  const { pixiRef, xRef, yRef, page, element, onNodeSelect } = useSelectionContext();
  const { current: pixi } = pixiRef;
  const getElements = useElementStore(({ getElements }) => getElements);
  const clientRef = useRef<[number, number]>(null);
  const draggingRef = useRef<ElementData[]>(null);

  useDifferences(page, {
    onRemove: ({ graph }) => pixi?.stage.removeChild(graph),
    onAdd: (element) => {
      const { uid, graph } = element;

      if (pixi) {
        pixi.stage.addChild(
          graph
            .on('pointertap', () => onNodeSelect('element', uid))
            .on('pointerdown', ({ clientX, clientY }) => {
              clientRef.current = [clientX, clientY];
              draggingRef.current = [element, ...getElements(uid, true)];
            }),
        );
      }
    },
    onFinally: (elements) => onGraphicsChange?.(elements.map(({ graph }) => graph)),
  });

  return useElementStore(
    ({ edit }) =>
      ({ clientX, clientY }: MouseEvent<HTMLCanvasElement>, finish = false) => {
        draggingRef.current?.forEach(({ uid, graph }) => {
          const highlight = pixi.stage.getChildByName(`~${uid}`);
          const movementX = clientX - clientRef.current[0];
          const movementY = clientY - clientRef.current[1];
          const positionX = graph.transform.position.x + movementX;
          const positionY = graph.transform.position.y + movementY;

          graph.setTransform(positionX, positionY);
          highlight?.setTransform(positionX, positionY);

          if (uid === element && xRef.current && yRef.current) {
            xRef.current.value = positionX.toString();
            yRef.current.value = positionY.toString();
          }

          if (finish) {
            edit(uid, { positionX, positionY });
          }

          return graph;
        });

        if (finish) {
          clientRef.current = null;
          draggingRef.current = null;
        } else {
          clientRef.current = [clientX, clientY];
        }
      },
  );
};
