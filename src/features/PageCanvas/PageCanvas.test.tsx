import IconButton from '@mui/material/IconButton';
import ue from '@testing-library/user-event';
import { EventBoundary, FederatedPointerEvent, Graphics } from 'pixi.js';
import { act } from 'react-dom/test-utils';

import PageCanvas from './PageCanvas';
import { SelectionProvider, useSelectionContext } from '~/contexts';
import { render, screen } from '~/jest.utils';
import { useElementStore } from '~/hooks';

const GeneratorButton: (props: { role: string }) => JSX.Element = ({ role }) => {
  const { onNodeSelect } = useSelectionContext();
  const add = useElementStore(({ add }) => add);

  return (
    <IconButton
      role={role}
      onClick={async () => {
        const superior = await add('page');

        await add({ superior, color: '#000000' });
        onNodeSelect('page', superior);
      }}
    />
  );
};

describe('features/PageCanvas', () => {
  it('render canvas', () => {
    render(
      <SelectionProvider>
        <PageCanvas />
      </SelectionProvider>,
    );

    const canvas = screen.getByTestId('canvas');

    expect(canvas).toBeInTheDocument();
    expect(canvas).toHaveAttribute('id');
  });

  it('render/dnd graphics', async () => {
    const user = ue.setup();
    let graphs: Graphics[];

    const handleGraphicsChange = jest.fn((graphics: Graphics[]) => {
      graphs = graphics;
    });

    render(
      <SelectionProvider>
        <GeneratorButton role="generator" />
        <PageCanvas onGraphicsChange={handleGraphicsChange} />
      </SelectionProvider>,
    );

    const canvas = screen.getByTestId('canvas');

    await act(() => user.click(screen.getByRole('generator')));
    expect(handleGraphicsChange).toBeCalled();
    expect(graphs.length).toBe(1);

    graphs[0].emit('pointerdown', new FederatedPointerEvent(new EventBoundary(graphs[0])));

    await act(() =>
      user.pointer([
        { target: canvas, keys: '[MouseLeft>]' },
        { target: canvas, coords: { x: 200, y: 200 } },
        { target: canvas, keys: '[/MouseLeft]' },
      ]),
    );

    const {
      transform: { position },
    } = graphs[0];

    expect(position.x).toBe(200);
    expect(position.y).toBe(200);
  });
});
