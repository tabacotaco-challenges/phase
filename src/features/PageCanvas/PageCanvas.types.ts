import { Graphics } from 'pixi.js';
import { PointerEvent } from 'react';

export type PixiAppHook = () => string;

export type DndHook = (
  onGraphicsChange?: PageCanvasProps['onGraphicsChange'],
) => (e: PointerEvent<HTMLCanvasElement>, finish?: boolean) => void;

export interface PageCanvasProps {
  onGraphicsChange?: (e: Graphics[]) => void;
}
