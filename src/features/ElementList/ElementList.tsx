import * as colors from '@mui/material/colors';
import AddIcon from '@mui/icons-material/Add';
import IconButton from '@mui/material/IconButton';
import { useCallback } from 'react';

import { ActionList, NoDataListItem, NodeItem } from '~/components';
import { ActivedProvider, ActivedProviderProps, useSelectionContext } from '~/contexts';
import { useElementStore } from '~/hooks';

export default function ElementList() {
  const { page: superior, element: selected, onNodeSelect } = useSelectionContext();

  const { elements, add } = useElementStore(
    ({ data, add }) => ({ elements: Array.from(data.get(superior)?.children || []), add }),
    ({ elements: e1 }, { elements: e2 }) => JSON.stringify(e1) === JSON.stringify(e2),
  );

  const handleRender: ActivedProviderProps['render'] = useCallback(
    (subList) =>
      !subList.length ? null : (
        <ActionList data-testid="collapse-list">
          {subList.map((uid, subIndex) => (
            <NodeItem
              key={uid}
              selected={selected}
              index={subIndex}
              uid={uid}
              onNodeSelect={onNodeSelect}
            />
          ))}
        </ActionList>
      ),
    [selected, onNodeSelect],
  );

  return (
    <ActionList
      data-testid="element-list"
      title="Elements"
      actions={
        <IconButton
          data-testid="add-element"
          color="primary"
          disabled={!Boolean(superior)}
          onClick={() => add({ superior, color: colors.grey[400] })}
        >
          <AddIcon />
        </IconButton>
      }
    >
      <NoDataListItem data-testid="no-data" shown={!elements.length} text="No Element" />

      {elements.map((uid, index) => (
        <ActivedProvider key={uid} render={handleRender}>
          <NodeItem selected={selected} index={index} uid={uid} onNodeSelect={onNodeSelect} />
        </ActivedProvider>
      ))}
    </ActionList>
  );
}
