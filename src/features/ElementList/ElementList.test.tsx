import ue from '@testing-library/user-event';
import { act } from 'react-dom/test-utils';

import ElementList from './ElementList';
import { PageList } from '../PageList';
import { SelectionProvider } from '~/contexts';
import { fireEvent, render, screen } from '~/jest.utils';

describe('features/ElementList', () => {
  it('render no-data', () => {
    render(<ElementList />);
    expect(screen.getByTestId('no-data')).toBeInTheDocument();
  });

  it('crud elements', async () => {
    const user = ue.setup();

    render(
      <SelectionProvider>
        <PageList />
        <ElementList />
      </SelectionProvider>
    );

    //* Add Element
    const elementList = screen.getByTestId('element-list');

    await act(() => user.click(screen.getByTestId('add-page')));
    await act(() => user.click(screen.getByTestId('add-element')));

    expect(elementList.querySelectorAll('.node-item').length).toBe(1);

    //* Update Description
    const e = { target: { value: 'ELEMENT' } };

    await act(() => user.dblClick(screen.getByTestId('element-item-0')));
    fireEvent.blur(screen.getByTestId('desc-editor'), e);

    expect(screen.getByTestId('element-display-0')).toHaveTextContent(
      e.target.value
    );

    //* Remove Element
    await act(() => user.click(screen.getByTestId('remove-element-0')));
    expect(elementList.querySelectorAll('.node-item').length).toBe(0);
  });

  it('crud sub-elements', async () => {
    const user = ue.setup();

    render(
      <SelectionProvider>
        <PageList />
        <ElementList />
      </SelectionProvider>
    );

    //* Add/Remove 2nd-level elements
    await act(() => user.click(screen.getByTestId('add-page')));
    await act(() => user.click(screen.getByTestId('add-element')));
    await act(() => user.click(screen.getByTestId('add-child-el')));

    const collapseList = screen.getByTestId('collapse-list');

    expect(screen.getByTestId('sub-collapse')).toBeInTheDocument();
    expect(screen.getByTestId('parent')).toBeInTheDocument();
    expect(collapseList).toBeInTheDocument();

    await act(() => user.click(screen.getByTestId('add-sibling-button')));
    expect(collapseList.querySelectorAll('.node-item').length).toBe(2);

    await act(() => user.click(screen.getByTestId('remove-element-1')));
    expect(collapseList.querySelectorAll('.node-item').length).toBe(1);

    //* Add 3rd-level element
    await act(() => user.click(screen.getByTestId('add-child-el')));
    expect(screen.getByTestId('grand')).toBeInTheDocument();

    //* Add 4rd-level element
    await act(() => user.click(screen.getByTestId('add-child-el')));
    expect(screen.getByTestId('grands')).toBeInTheDocument();
    expect(collapseList.querySelectorAll('.node-item').length).toBe(1);

    //* Breadcrumbs Test
    await act(() => user.click(screen.getByTestId('grands')));

    const grandsMenu = screen.getByTestId('grands-menu');

    expect(grandsMenu).toBeInTheDocument();
    expect(grandsMenu.querySelectorAll('.grand-item').length).toBe(2);

    await act(() => user.click(screen.getByTestId('grand-item-0')));
    expect(collapseList.querySelectorAll('.node-item').length).toBe(1);
  });
});
