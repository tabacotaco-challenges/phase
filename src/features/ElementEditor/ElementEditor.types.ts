import { ElementData } from '~/hooks';

type FieldName = keyof Pick<
  ElementData,
  'positionX' | 'positionY' | 'opacity' | 'color'
>;

export type HandleChangeEventHandler = <T extends FieldName>(e: {
  target: {
    name: T;
    value: ElementData[T];
  };
}) => void;

export type GeneratorButton = (props: { role: string }) => JSX.Element;
