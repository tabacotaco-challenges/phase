import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';

import * as Types from './ElementEditor.types';
import { ElementData, useElementStore } from '~/hooks';
import { useSelectionContext } from '~/contexts';

import {
  ActionList,
  ColorPicker,
  NoDataListItem,
  NumberField,
  NumberSliderField,
} from '~/components';

export default function ElementEditor() {
  const { xRef, yRef, element: uid } = useSelectionContext();

  const { element, edit } = useElementStore(
    ({ data, edit, remove }) => ({
      element: data.get(uid) as ElementData,
      edit,
      remove,
    }),
    ({ element: e1 }, { element: e2 }) =>
      e1?.positionX === e2?.positionX &&
      e1?.positionY === e2?.positionY &&
      e1?.opacity === e2?.opacity &&
      e1?.color === e2?.color,
  );

  const handleChange: Types.HandleChangeEventHandler = ({ target: { name, value } }) =>
    edit(element.uid, { [name]: value });

  return (
    <ActionList key={element?.uid} title="Properties">
      <NoDataListItem data-testid="no-data" shown={!element} text="No Selected Element" />

      {element && (
        <>
          <ListItem>
            <ListItemText
              disableTypography
              primary={
                <NumberField
                  fullWidth
                  inputRef={xRef}
                  size="small"
                  variant="outlined"
                  label="Position X"
                  name="positionX"
                  data-testid="positionX"
                  step={10}
                  value={element.positionX}
                  onChange={handleChange}
                />
              }
            />
          </ListItem>

          <ListItem>
            <ListItemText
              disableTypography
              primary={
                <NumberField
                  fullWidth
                  inputRef={yRef}
                  size="small"
                  variant="outlined"
                  label="Position Y"
                  name="positionY"
                  data-testid="positionY"
                  step={10}
                  value={element.positionY}
                  onChange={handleChange}
                />
              }
            />
          </ListItem>

          <ListItem>
            <ListItemText
              disableTypography
              primary={
                <NumberSliderField
                  fullWidth
                  size="small"
                  variant="outlined"
                  label="Opacity"
                  name="opacity"
                  data-testid="opacity"
                  value={element.opacity}
                  onChange={handleChange}
                  SliderProps={{
                    min: 0,
                    max: 1,
                    step: 0.001,
                    valueLabelDisplay: 'auto',
                  }}
                />
              }
            />
          </ListItem>

          <ListItem>
            <ListItemText
              disableTypography
              primary={
                <ColorPicker
                  fullWidth
                  size="small"
                  variant="outlined"
                  label="Color"
                  name="color"
                  data-testid="color"
                  value={element.color}
                  onChange={handleChange}
                />
              }
            />
          </ListItem>
        </>
      )}
    </ActionList>
  );
}
