import IconButton from '@mui/material/IconButton';
import ue from '@testing-library/user-event';
import { act } from 'react-dom/test-utils';

import * as Types from './ElementEditor.types';
import ElementEditor from './ElementEditor';
import { SelectionProvider, useSelectionContext } from '~/contexts';
import { render, screen, fireEvent } from '~/jest.utils';
import { useElementStore } from '~/hooks';

const GeneratorButton: Types.GeneratorButton = ({ role }) => {
  const add = useElementStore(({ add }) => add);
  const { onNodeSelect } = useSelectionContext();

  return (
    <IconButton
      role={role}
      onClick={async () => {
        const superior = await add('page');
        const uid = await add({ superior, color: '#000000' });

        onNodeSelect('element', uid);
      }}
    />
  );
};

describe('features/ElementEditor', () => {
  it('render no-data', () => {
    render(<ElementEditor />);

    expect(screen.getByTestId('no-data')).toBeInTheDocument();
  });

  it('render editor', async () => {
    const user = ue.setup();

    render(
      <SelectionProvider>
        <GeneratorButton role="generator" />
        <ElementEditor />
      </SelectionProvider>
    );

    await act(() => user.click(screen.getByRole('generator')));

    expect(screen.getByTestId('positionX')).toBeInTheDocument();
    expect(screen.getByTestId('positionY')).toBeInTheDocument();
    expect(screen.getByTestId('opacity')).toBeInTheDocument();
    expect(screen.getByTestId('color')).toBeInTheDocument();
  });

  it('update values', async () => {
    const user = ue.setup();

    render(
      <SelectionProvider>
        <GeneratorButton role="generator" />
        <ElementEditor />
      </SelectionProvider>
    );

    await act(() => user.click(screen.getByRole('generator')));

    //* Positions
    const positionX = screen.getByTestId('positionX').querySelector('input');
    const positionY = screen.getByTestId('positionY').querySelector('input');

    await act(() => user.type(positionX, '200'));
    expect(positionX).toHaveValue('200');

    await act(() => user.type(positionY, '300'));
    expect(positionY).toHaveValue('300');

    //* Opacity
    const opacity = screen.getByTestId('opacity').querySelector('input');

    fireEvent.change(opacity, { target: { name: 'opacity', value: '0.43' } });
    expect(opacity).toHaveValue('0.43');

    //* Color Picker
    const color = screen.getByTestId('color').querySelector('input');

    fireEvent.change(color, { target: { name: 'color', value: '#ff00ff' } });
    expect(color).toHaveValue('#ff00ff');
  });
});
