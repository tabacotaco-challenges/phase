import ue from '@testing-library/user-event';
import { act } from 'react-dom/test-utils';

import PageList from './PageList';
import { SelectionProvider } from '~/contexts';
import { fireEvent, render, screen } from '~/jest.utils';

describe('features/PageList', () => {
  it('render no-data', () => {
    render(<PageList />);
    expect(screen.getByTestId('no-data')).toBeInTheDocument();
  });

  it('crud of page', async () => {
    const user = ue.setup();

    render(
      <SelectionProvider>
        <PageList />
      </SelectionProvider>
    );

    //* Add Page
    const pageList = screen.getByTestId('page-list');

    await act(() => user.click(screen.getByTestId('add-page')));
    expect(pageList.querySelectorAll('.node-item').length).toBe(1);

    //* Update Description
    const e = { target: { value: 'PAGE' } };

    await act(() => user.dblClick(screen.getByTestId('page-item-0')));
    fireEvent.blur(screen.getByTestId('desc-editor'), e);

    expect(screen.getByTestId('page-display-0')).toHaveTextContent(
      e.target.value
    );

    //* Remove Page
    await act(() => user.click(screen.getByTestId('remove-page-0')));
    expect(pageList.querySelectorAll('.node-item').length).toBe(0);
  });
});
