import AddIcon from '@mui/icons-material/Add';
import IconButton from '@mui/material/IconButton';

import { ActionList, NoDataListItem, NodeItem } from '~/components';
import { useElementStore } from '~/hooks';
import { useSelectionContext } from '~/contexts';

export default function PageList() {
  const { page: selected, onNodeSelect } = useSelectionContext();

  const { pages, add } = useElementStore(
    ({ data, add }) => ({
      add,
      pages: Array.from(data.values()).reduce<string[]>(
        (result, { type, uid }) => (type !== 'page' ? result : result.concat(uid)),
        [],
      ),
    }),
    ({ pages: p1 }, { pages: p2 }) => JSON.stringify(p1) === JSON.stringify(p2),
  );

  return (
    <ActionList
      data-testid="page-list"
      title="Pages"
      actions={
        <IconButton
          data-testid="add-page"
          color="primary"
          onClick={async () => onNodeSelect('page', await add('page'))}
        >
          <AddIcon />
        </IconButton>
      }
    >
      <NoDataListItem data-testid="no-data" shown={!pages.length} text="No Page" />

      {pages.map((uid, i) => (
        <NodeItem key={uid} selected={selected} index={i} uid={uid} onNodeSelect={onNodeSelect} />
      ))}
    </ActionList>
  );
}
