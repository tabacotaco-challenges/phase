import ListItem from '@mui/material/ListItem';
import ListItemText from '@mui/material/ListItemText';

import * as Types from './NoDataListItem.types';

export default function NoDataListItem({
  shown,
  text,
  ...props
}: Types.NoDataListItemProps) {
  return !shown ? null : (
    <ListItem {...props}>
      <ListItemText
        primaryTypographyProps={{
          variant: 'subtitle1',
          color: 'text.secondary',
          align: 'center',
        }}
        primary={text || 'No Data'}
      />
    </ListItem>
  );
}
