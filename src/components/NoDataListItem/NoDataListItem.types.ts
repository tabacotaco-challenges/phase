import { ListItemProps } from '@mui/material/ListItem';

export interface NoDataListItemProps extends ListItemProps {
  shown: boolean;
  text?: string;
}
