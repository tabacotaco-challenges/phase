import AddIcon from '@mui/icons-material/Add';
import IconButton from '@mui/material/IconButton';
import InputAdornment from '@mui/material/InputAdornment';
import RemoveIcon from '@mui/icons-material/Remove';
import TextField from '@mui/material/TextField';

import FormatInput from './FormatInput';
import * as Types from './NumberField.types';

export default function NumberField<N extends string>({
  InputLabelProps,
  InputProps,

  max,
  min,
  step = 1,
  value,
  onChange,
  ...props
}: Types.NumberFieldProps<N>) {
  const num = Number.parseFloat(value as string) || 0;

  const handleChange = (e: string | number) =>
    onChange({
      target: {
        name: props.name,
        value: Math.min(
          min || Number.MAX_SAFE_INTEGER,
          Math.max(
            max || Number.MIN_SAFE_INTEGER,
            Number.parseFloat(e as string) || 0
          )
        ),
      },
    });

  return (
    <TextField
      variant="outlined"
      {...props}
      value={value}
      onChange={(e) => handleChange(e.target.value)}
      InputLabelProps={{ ...InputLabelProps, shrink: true }}
      InputProps={
        {
          ...InputProps,
          inputComponent: FormatInput,
          inputProps: { max, min, inputMode: 'numeric', pattern: '[0-9]*' },
          endAdornment: (
            <InputAdornment position="end">
              <IconButton size="small" onClick={() => handleChange(num + step)}>
                <AddIcon />
              </IconButton>

              <IconButton size="small" onClick={() => handleChange(num - step)}>
                <RemoveIcon />
              </IconButton>
            </InputAdornment>
          ),
        } as object
      }
    />
  );
}
