import { TextFieldProps } from '@mui/material/TextField';

export interface NumberFieldProps<N extends string>
  extends Omit<
    TextFieldProps,
    | 'defaultValue'
    | 'max'
    | 'min'
    | 'name'
    | 'onChange'
    | 'inputProps'
    | 'InputProps'
    | 'InputLabelProps'
  > {
  min?: number;
  max?: number;
  name: N;
  step?: number;

  onChange?: (e: { target: { name: N; value: number } }) => void;

  InputLabelProps?: Omit<TextFieldProps['InputLabelProps'], 'shrink'>;

  InputProps?: Omit<
    TextFieldProps['InputProps'],
    'inputComponent' | 'inputProps' | 'endAdornment'
  >;
}

export interface FormatInputProps {
  name?: string;
  onChange: (e: { target: { name?: string; value: number } }) => void;
}
