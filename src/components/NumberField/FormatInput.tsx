import { NumericFormat, NumericFormatProps } from 'react-number-format';
import { forwardRef } from 'react';

import * as Types from './NumberField.types';

export default forwardRef<NumericFormatProps, Types.FormatInputProps>(
  function FormatInput({ onChange, ...props }, ref) {
    return (
      <NumericFormat
        {...props}
        thousandSeparator
        valueIsNumericString
        getInputRef={ref}
        onValueChange={({ value }: { value: string }) =>
          onChange({
            target: {
              name: props.name,
              value: Number.parseFloat(value) || 0,
            },
          })
        }
      />
    );
  }
);
