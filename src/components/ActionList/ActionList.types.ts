import { ListProps } from '@mui/material/List';
import { ReactNode } from 'react';

export interface ActionListProps extends Omit<ListProps, 'subheader'> {
  actions?: ReactNode;
  title?: string;
}
