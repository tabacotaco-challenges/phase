import Typography from '@mui/material/Typography';

import { ArcListSubheader, GapList, SpaceBetweenToolbar } from '~/styles';
import * as Types from './ActionList.types';

export default function ActionList({
  actions,
  title,
  ...props
}: Types.ActionListProps) {
  const disableSubheader = Boolean(!actions && !title);

  return (
    <GapList
      {...props}
      subheader={
        !disableSubheader && (
          <ArcListSubheader data-testid="subheader">
            <SpaceBetweenToolbar disableGutters variant="dense">
              <Typography variant="subtitle1" color="text.secondary">
                {title}
              </Typography>

              {actions}
            </SpaceBetweenToolbar>
          </ArcListSubheader>
        )
      }
    />
  );
}
