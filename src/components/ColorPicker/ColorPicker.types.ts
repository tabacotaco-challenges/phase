import { TextFieldProps } from '@mui/material/TextField';

export interface ColorPickerProps<N extends string>
  extends Omit<
    TextFieldProps,
    | 'type'
    | 'name'
    | 'defaultValue'
    | 'onChange'
    | 'InputLabelProps'
    | 'InputProps'
  > {
  name: N;
  onChange?: (e: { target: { name: N; value: string } }) => void;

  InputLabelProps?: Omit<TextFieldProps['InputLabelProps'], 'shrink'>;
  InputProps?: Omit<TextFieldProps['InputProps'], 'endAdornment'>;
}
