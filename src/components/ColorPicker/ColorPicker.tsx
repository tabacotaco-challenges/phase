import InputAdornment from '@mui/material/InputAdornment';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import TextField, { TextFieldProps } from '@mui/material/TextField';
import Tooltip from '@mui/material/Tooltip';
import { debounce } from 'throttle-debounce';

import * as Types from './ColorPicker.types';

export default function ColorPicker<N extends string>({
  InputLabelProps,
  InputProps,

  value,
  onChange,
  ...props
}: Types.ColorPickerProps<N>) {
  return (
    <TextField
      variant="outlined"
      {...props}
      {...({ onChange: debounce(100, onChange) } as object)}
      type="color"
      defaultValue={value}
      InputLabelProps={{ ...InputLabelProps, shrink: true }}
      InputProps={{
        ...InputProps,
        endAdornment: (
          <InputAdornment position="end">
            <Tooltip title={value.toString().toUpperCase()}>
              <InfoOutlinedIcon />
            </Tooltip>
          </InputAdornment>
        ),
      }}
    />
  );
}
