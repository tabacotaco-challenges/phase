import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import ChevronLeftIcon from '@mui/icons-material/ChevronLeft';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import Divider from '@mui/material/Divider';
import IconButton from '@mui/material/IconButton';
import Typography from '@mui/material/Typography';

import { AbsoluteDrawer, SpaceBetweenToolbar } from '~/styles';
import * as Types from './PersistentDrawer.types';

export default function PersistentDrawer({
  anchor,
  children,
  disableToolbar = false,
  maxWidth,
  title,
  onClose,
  ...props
}: Types.PersistentDrawerProps) {
  return (
    <AbsoluteDrawer
      {...props}
      anchor={anchor}
      maxWidth={maxWidth}
      PaperProps={{ elevation: 3 }}
    >
      {!disableToolbar && (
        <>
          <AppBar position="sticky" color="transparent" elevation={0}>
            <SpaceBetweenToolbar variant="dense">
              <Typography variant="subtitle1">{title}</Typography>

              <IconButton onClick={onClose}>
                {anchor === 'left' ? <ChevronLeftIcon /> : <ChevronRightIcon />}
              </IconButton>
            </SpaceBetweenToolbar>
          </AppBar>

          <Divider />
        </>
      )}

      <Box padding={(theme) => theme.spacing(1, 2)}>{children}</Box>
    </AbsoluteDrawer>
  );
}
