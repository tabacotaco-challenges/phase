import { DrawerProps } from '@mui/material/Drawer';

export interface PersistentDrawerProps
  extends Omit<DrawerProps, 'anchor' | 'open' | 'variant'> {
  anchor: 'left' | 'right';
  disableToolbar?: boolean;
  maxWidth: number | string;
  open: boolean;
  title?: string;

  onClose: () => void;
}
