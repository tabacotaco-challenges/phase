import IconButton from '@mui/material/IconButton';
import KeyboardArrowDownIcon from '@mui/icons-material/KeyboardArrowDown';
import KeyboardArrowRightIcon from '@mui/icons-material/KeyboardArrowRight';

import * as Types from './CollapsedButton.types';

export default function CollapsedButton({
  collapsed,
  ...props
}: Types.CollapsedButtonProps) {
  return (
    <IconButton {...props}>
      {collapsed ? <KeyboardArrowRightIcon /> : <KeyboardArrowDownIcon />}
    </IconButton>
  );
}
