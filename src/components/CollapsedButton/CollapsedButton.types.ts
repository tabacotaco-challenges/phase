import { IconButtonProps } from '@mui/material/IconButton';

export interface CollapsedButtonProps
  extends Omit<IconButtonProps, 'children'> {
  collapsed: boolean;
}
