import Slider from '@mui/material/Slider';
import Stack from '@mui/material/Stack';
import { forwardRef, useState } from 'react';

import * as Types from './NumberSliderField.types';

export default forwardRef<HTMLDivElement, Types.SliderInputProps>(
  function SliderInput({ className, value, onChange, ...props }, ref) {
    const [num, setNum] = useState(value as number);

    return (
      <Stack
        direction="row"
        justifyContent="center"
        alignItems="center"
        paddingRight={(theme) => `${theme.spacing(3)} !important`}
        className={className}
      >
        <Slider
          {...props}
          ref={ref}
          value={num}
          onChange={(_e, newValue) => {
            const newNum = newValue as number;

            setNum(newNum);

            onChange({
              target: {
                name: props.name,
                value: newNum,
              },
            });
          }}
        />
      </Stack>
    );
  }
);
