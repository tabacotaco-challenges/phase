import TextField from '@mui/material/TextField';
import { debounce } from 'throttle-debounce';

import SliderInputProps from './SliderInput';
import * as Types from './NumberSliderField.types';

export default function NumberSliderField<N extends string>({
  InputLabelProps,
  InputProps,
  SliderProps,

  value,
  onChange,
  ...props
}: Types.NumberSliderFieldProps<N>) {
  const num = Number.parseFloat(value as string) || 0;

  return (
    <TextField
      variant="outlined"
      {...props}
      InputLabelProps={{ ...InputLabelProps, shrink: true }}
      InputProps={
        {
          ...InputProps,
          inputComponent: SliderInputProps,
          inputProps: {
            ...SliderProps,
            value: num,
            onChange: debounce(100, onChange),
          },
        } as object
      }
    />
  );
}
