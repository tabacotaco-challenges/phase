import { SliderProps } from '@mui/material/Slider';
import { TextFieldProps } from '@mui/material/TextField';
import { ChangeEventHandler } from 'react';

export interface NumberSliderFieldProps<N extends string>
  extends Omit<
    TextFieldProps,
    | 'defaultValue'
    | 'name'
    | 'onChange'
    | 'inputProps'
    | 'InputProps'
    | 'InputLabelProps'
  > {
  name: N;
  onChange?: (e: { target: { name: N; value: number } }) => void;

  InputLabelProps?: Omit<TextFieldProps['InputLabelProps'], 'shrink'>;

  InputProps?: Omit<
    TextFieldProps['InputProps'],
    'inputComponent' | 'inputProps'
  >;

  SliderProps?: Omit<
    SliderProps,
    'orientation' | 'defaultValue' | 'value' | 'onChange'
  >;
}

export interface SliderInputProps extends Omit<SliderProps, 'onChange'> {
  onChange: (e: { target: { name?: string; value: number } }) => void;
}
