import { SelectionContextValue } from '~/contexts';

export interface NodeItemProps {
  uid: string;
  index: number;
  selected?: string;
  onNodeSelect: SelectionContextValue['onSelect'];
}
