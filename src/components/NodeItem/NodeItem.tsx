import * as colors from '@mui/material/colors';
import AddIcon from '@mui/icons-material/Add';
import IconButton from '@mui/material/IconButton';
import ListItemIcon from '@mui/material/ListItemIcon';
import ListItemSecondaryAction from '@mui/material/ListItemSecondaryAction';
import RemoveIcon from '@mui/icons-material/Remove';
import TextField from '@mui/material/TextField';
import Typography from '@mui/material/Typography';
import { useState } from 'react';

import * as Types from './NodeItem.types';
import { ArcListItemButton, ActionableListItemText } from '~/styles';
import { CollapsedButton } from '../CollapsedButton';
import { NodeCollapse, getDisplayName } from '../NodeCollapse';
import { useActivedContext } from '~/contexts';
import { useElementStore } from '~/hooks';

export default function NodeItem({ selected, index, uid, onNodeSelect }: Types.NodeItemProps) {
  const [editing, setEditing] = useState(false);

  const { superior, node, add, edit, remove } = useElementStore(
    ({ data, add, edit, remove }) => {
      const node = data.get(uid);

      return {
        superior: data.get(node.superior),
        node,
        add,
        edit,
        remove,
      };
    },
    ({ node: { description: d1, children: c1 } }, { node: { description: d2, children: c2 } }) => {
      const { size: s1 = 0 } = c1 || {};
      const { size: s2 = 0 } = c2 || {};

      return d1 === d2 && s1 === s2;
    },
  );

  const displayName = getDisplayName({ ...node, index });
  const collapse = useActivedContext(node);
  const { collapsed, onActivedChange } = collapse;

  const handleDescChange = (newDesc: string) => {
    setEditing(false);
    edit(node.uid, { description: newDesc });
  };

  return (
    <>
      <ArcListItemButton
        data-testid={`${node.type}-item-${index}`}
        className="node-item"
        selected={selected === node.uid}
        onDoubleClick={() => setEditing(true)}
        onClick={() => onNodeSelect(node.type, node.uid === selected ? null : node.uid)}
      >
        {node.type === 'element' && (
          <ListItemIcon onClick={(e) => e.stopPropagation()}>
            {!node.children?.size ? (
              <IconButton
                data-testid="add-child-el"
                color="primary"
                onClick={async () => {
                  await add({
                    superior: node.uid,
                    color: colors.grey[400],
                  });

                  onActivedChange(node.uid, true);
                }}
              >
                <AddIcon />
              </IconButton>
            ) : (
              <CollapsedButton collapsed={collapsed} onClick={() => onActivedChange(node.uid)} />
            )}
          </ListItemIcon>
        )}

        <ActionableListItemText
          disableTypography
          withAction
          primary={
            !editing ? (
              <Typography
                variant="subtitle1"
                color="text.primary"
                data-testid={`${node.type}-display-${index}`}
              >
                {displayName}
              </Typography>
            ) : (
              <TextField
                fullWidth
                autoFocus
                variant="outlined"
                size="small"
                inputProps={{ 'data-testid': 'desc-editor' }}
                label={`Description (${displayName})`}
                defaultValue={node.description}
                onBlur={(e) => handleDescChange(e.target.value)}
                onKeyDown={(e) => {
                  if (e.key === 'Enter') {
                    handleDescChange((e.target as HTMLInputElement).value);
                  }
                }}
              />
            )
          }
        />

        <ListItemSecondaryAction>
          <IconButton
            data-testid={`remove-${node.type}-${index}`}
            color="secondary"
            onClick={async () => {
              await remove(node.uid);
              onNodeSelect(node.type, null);

              if (!superior?.children?.size) {
                onActivedChange(superior?.superior);
              }
            }}
          >
            <RemoveIcon />
          </IconButton>
        </ListItemSecondaryAction>
      </ArcListItemButton>

      {superior?.type === 'page' && node.type === 'element' && <NodeCollapse {...collapse} />}
    </>
  );
}
