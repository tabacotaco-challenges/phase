import * as colors from '@mui/material/colors';
import AddIcon from '@mui/icons-material/Add';
import Breadcrumbs from '@mui/material/Breadcrumbs';
import IconButton from '@mui/material/IconButton';
import Menu from '@mui/material/Menu';
import MenuItem from '@mui/material/MenuItem';
import Typography from '@mui/material/Typography';
import { useState } from 'react';

import * as Types from './NodeCollapse.types';
import * as utils from './NodeCollapse.utils';
import { SpaceBetweenToolbar, SubCollapse, TextButton } from '~/styles';
import { useElementStore } from '~/hooks';

export default function NodeCollapse({
  collapsed,
  children,
  breadcrumbs,
  onActivedChange,
}: Types.NodeCollapseProps) {
  const [anchorEl, setAnchorEl] = useState<HTMLElement>(null);
  const list = breadcrumbs.slice(0, breadcrumbs.length - 1);
  const last = breadcrumbs[breadcrumbs.length - 1];

  const add = useElementStore(({ add }) => add);

  return !children ? null : (
    <SubCollapse data-testid="sub-collapse" in={!collapsed}>
      <SpaceBetweenToolbar
        variant="dense"
        sx={(theme) => ({
          minHeight: 'auto',
          padding: `${theme.spacing(0, 2)} !important`,
        })}
      >
        <Breadcrumbs separator="›" onClick={(e) => e.stopPropagation()}>
          {list.length === 1 && (
            <TextButton
              data-testid="grand"
              variant="text"
              size="small"
              onClick={() => onActivedChange(list[0].uid, true)}
            >
              <Typography variant="caption" color="text.primary">
                {utils.getDisplayName(list[0])}
              </Typography>
            </TextButton>
          )}

          {list.length > 1 && (
            <TextButton
              data-testid="grands"
              variant="text"
              size="small"
              onClick={(e) => setAnchorEl(e.currentTarget)}
            >
              <Typography variant="caption" color="text.primary">
                {`...(${list.length})`}
              </Typography>
            </TextButton>
          )}

          <Typography variant="caption" color="text.secondary" data-testid="parent">
            {utils.getDisplayName(last)}
          </Typography>
        </Breadcrumbs>

        <IconButton
          data-testid="add-sibling-button"
          color="primary"
          onClick={() =>
            add({
              superior: last.uid,
              color: colors.grey[400],
            })
          }
        >
          <AddIcon />
        </IconButton>
      </SpaceBetweenToolbar>

      <Menu
        data-testid="grands-menu"
        anchorEl={anchorEl}
        open={Boolean(anchorEl)}
        onClose={() => setAnchorEl(null)}
      >
        {list.reverse().map((breadcrumb, i) => (
          <MenuItem
            key={breadcrumb.uid}
            data-testid={`grand-item-${i}`}
            className="grand-item"
            onClick={() => {
              onActivedChange(breadcrumb.uid, true);
              setAnchorEl(null);
            }}
          >
            {utils.getDisplayName(breadcrumb)}
          </MenuItem>
        ))}
      </Menu>

      {children}
    </SubCollapse>
  );
}
