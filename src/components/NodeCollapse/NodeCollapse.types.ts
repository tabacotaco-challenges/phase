import { ActivedContextHookResult, ElementDataWithIndex } from '~/contexts';

export interface NodeCollapseProps extends Omit<ActivedContextHookResult, 'children'> {
  children?: ActivedContextHookResult['children'];
}

export type GetDisplayNameFn = (node: ElementDataWithIndex) => string;
