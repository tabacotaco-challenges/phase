import * as Types from './NodeCollapse.types';

export const getDisplayName: Types.GetDisplayNameFn = ({ type, description, index }) => {
  const prefix = type === 'page' ? 'Page' : 'Element';

  return description || `${prefix} ${index}`;
};
