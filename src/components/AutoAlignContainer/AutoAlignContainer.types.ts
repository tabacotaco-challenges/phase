import { ContainerProps } from '@mui/material/Container';

export interface AutoAlignContainerProps
  extends Omit<ContainerProps, 'disableGutters' | 'maxWidth' | 'sx'> {
  spacingWidth: number | string;
  enableLeftSpacing?: boolean;
  enableRightSpacing?: boolean;
}
