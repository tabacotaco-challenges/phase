import Container from '@mui/material/Container';

import * as Types from './AutoAlignContainer.types';
import { useWidth } from '~/hooks';

export default function AutoAlignContainer({
  children,
  spacingWidth,
  enableLeftSpacing = false,
  enableRightSpacing = false,
  ...props
}: Types.AutoAlignContainerProps) {
  const width = useWidth();

  const spacing = typeof spacingWidth === 'string' ? spacingWidth : `${spacingWidth}px`;

  const multiple = [enableLeftSpacing, enableRightSpacing].filter((enabled) => enabled).length;

  return (
    <Container
      {...props}
      disableGutters
      maxWidth={false}
      sx={(theme) => ({
        height: `calc(${window.innerHeight}px - ${theme.spacing(2)})`,
        padding: theme.spacing(0, 1),

        ...(width !== 'xs' && {
          width: `calc(100% - (${spacing} * ${multiple}))`,
          margin: 0,
          marginLeft: enableLeftSpacing ? spacing : null,
          marginRight: enableRightSpacing ? spacing : null,
        }),
      })}
    >
      {children}
    </Container>
  );
}
