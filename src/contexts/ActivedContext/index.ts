export { default as ActivedProvider } from './ActivedContext';
export { useActivedContext } from './ActivedContext.hooks';

export type {
  ActivedContextHookResult,
  ActivedProviderProps,
  ElementDataWithIndex,
} from './ActivedContext.types';
