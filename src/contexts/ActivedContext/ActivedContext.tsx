import { ActivedContext, useActivedContextValue } from './ActivedContext.hooks';
import * as Types from './ActivedContext.types';

export default function ActivedProvider({
  children,
  render,
}: Types.ActivedProviderProps) {
  const value = useActivedContextValue(render);

  return (
    <ActivedContext.Provider value={value}>{children}</ActivedContext.Provider>
  );
}
