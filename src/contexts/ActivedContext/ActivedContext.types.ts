import { Dispatch, ReactNode, SetStateAction } from 'react';
import { ElementData, PageData } from '~/hooks';

export type Value = string | null;

export type ElementDataWithIndex = (PageData | ElementData) & { index: number };

export interface ActivedContextValue {
  actived: Value;
  setActived: Dispatch<SetStateAction<Value>>;
  render: (elements: string[]) => ReactNode;
}

export interface ActivedProviderProps extends Pick<ActivedContextValue, 'render'> {
  children: ReactNode;
}

export interface ActivedContextHookResult {
  breadcrumbs: ElementDataWithIndex[];
  children: ReactNode;
  collapsed: boolean;
  onActivedChange: (uid: Value, force?: boolean) => void;
}

export type GetBreadcrumsFn = (uid: string) => ElementDataWithIndex[];

export type ActivedContextValueHook = (
  render: ActivedContextValue['render'],
) => ActivedContextValue;

export type ActivedContextHook = (node?: ElementData | PageData) => ActivedContextHookResult;
