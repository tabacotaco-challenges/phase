import { createContext, useContext, useMemo, useState } from 'react';

import { useElementStore } from '~/hooks';
import * as Types from './ActivedContext.types';

export const ActivedContext = createContext<Types.ActivedContextValue>({
  actived: null,
  setActived: () => null,
  render: () => null,
});

export const useActivedContextValue: Types.ActivedContextValueHook = (render) => {
  const [actived, setActived] = useState<Types.Value>(null);

  return useMemo(
    () => ({
      actived,
      setActived,
      render,
    }),
    [actived, setActived, render],
  );
};

export const useActivedContext: Types.ActivedContextHook = ({ uid, type }) => {
  const { actived, setActived, render } = useContext(ActivedContext);

  const elements = useElementStore(
    ({ data }) => Array.from(data.get(actived)?.children || []),
    (e1, e2) => JSON.stringify(e1) === JSON.stringify(e2),
  );

  const breadcrumbs = useElementStore(
    ({ data, getElements }) => {
      const getBreadcrumbs: Types.GetBreadcrumsFn = (uid) => {
        const target = data.get(uid);
        const siblings = getElements(target?.superior);

        return target?.type !== 'element'
          ? []
          : [...getBreadcrumbs(target.superior), { ...target, index: siblings.indexOf(target) }];
      };

      return getBreadcrumbs(actived);
    },
    (b1, b2) => {
      const m2 = new Map(b2.map(({ uid, description }) => [uid, description]));

      return (
        b1.every(({ uid, description }) => m2.get(uid) === description && m2.delete(uid)) &&
        m2.size === 0
      );
    },
  );

  const collapsed = type !== 'element' ? true : !breadcrumbs.some((el) => el.uid === uid);

  return {
    breadcrumbs,
    children: render(collapsed ? [] : elements),
    collapsed,

    onActivedChange: (newUid, force = false) => setActived(!force && !collapsed ? null : newUid),
  };
};
