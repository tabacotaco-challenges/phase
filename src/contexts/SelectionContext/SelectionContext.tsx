import * as Types from './SelectionContext.types';

import {
  SelectionContext,
  useGraphicsGearing,
  useSelectionValue,
  useSelectionHighlight,
} from './SelectionContext.hooks';

export default function SelectionProvider({
  children,
  onSelect,
}: Types.SelectionProviderProps) {
  const value = useSelectionValue(onSelect);

  useSelectionHighlight(value);
  useGraphicsGearing(value);

  return (
    <SelectionContext.Provider value={value}>
      {children}
    </SelectionContext.Provider>
  );
}
