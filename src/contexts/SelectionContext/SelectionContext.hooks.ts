import { Application, Graphics } from 'pixi.js';
import { useTheme } from '@mui/material/styles';

import {
  createContext,
  useCallback,
  useContext,
  useEffect,
  useMemo,
  useRef,
  useState,
} from 'react';

import * as Types from './SelectionContext.types';
import { ElementData, useDifferences, useElementStore } from '~/hooks';

export const SelectionContext = createContext<Types.SelectionContextValue>({
  pixiRef: null,
  xRef: null,
  yRef: null,

  selecteds: { page: null, element: null },
  setSelecteds: () => null,
  onSelect: null,
});

export const useSelectionValue: Types.SelectionValueHook = (onSelect) => {
  const pixiRef = useRef<Application>(null);
  const xRef = useRef<HTMLInputElement>(null);
  const yRef = useRef<HTMLInputElement>(null);

  const [selecteds, setSelecteds] = useState<Types.SelectionContextValue['selecteds']>({
    page: null,
    element: null,
  });

  return useMemo(
    () => ({
      pixiRef,
      xRef,
      yRef,

      selecteds,
      setSelecteds,
      onSelect,
    }),
    [pixiRef, xRef, yRef, selecteds, setSelecteds, onSelect],
  );
};

export const useSelectionContext: Types.SelectionContextHook = () => {
  const {
    pixiRef,
    xRef,
    yRef,

    selecteds: { page, element },
    setSelecteds,
    onSelect,
  } = useContext(SelectionContext);

  return {
    pixiRef,
    xRef,
    yRef,
    page,
    element,

    onNodeSelect: useCallback(
      (type, uid) => {
        setSelecteds((selected) => ({
          ...(type === 'element' && selected),
          [type]: uid,
        }));

        onSelect?.(type, uid);
      },
      [setSelecteds, onSelect],
    ),
  };
};

export const useSelectionHighlight: Types.SelectionHighlightHook = ({
  pixiRef,
  selecteds: { element },
}) => {
  const theme = useTheme();
  const graphicsRef = useRef(new Map<string, Graphics>());
  const { current: pixi } = pixiRef;

  useDifferences(element, {
    includeSelf: true,
    onAdd: ({ uid, positionX, positionY }) => {
      const name = `~${uid}`;

      if (pixi && !pixi.stage.getChildByName(name)) {
        const highlight = new Graphics();

        const color = Number.parseInt(theme.palette.primary.main.substring(1), 16);

        highlight.name = name;

        highlight
          .setTransform(positionX, positionY)
          .beginFill(color, 0)
          .lineStyle(2, color, 1)
          .drawRect(0, 0, 100, 100)
          .endFill();

        graphicsRef.current.set(uid, highlight);
        pixi.stage.addChild(highlight);
      }
    },
    onRemove: ({ uid }) => {
      if (graphicsRef.current.has(uid)) {
        const highlight = graphicsRef.current.get(uid);

        pixi?.stage.removeChild(highlight);
        graphicsRef.current.delete(uid);
      }
    },
  });
};

export const useGraphicsGearing: Types.GraphicsGearingHook = ({
  pixiRef,
  selecteds: { element },
}) => {
  const { current: pixi } = pixiRef;

  const movedEl = useElementStore(
    ({ data }) => data.get(element) as ElementData,
    (e1, e2) => {
      if (e1 && e2) {
        const { positionX: x1, positionY: y1 } = e1;
        const { positionX: x2, positionY: y2 } = e2;

        return x1 === x2 && y1 === y2;
      }

      return e1?.uid === e2?.uid;
    },
  );

  useEffect(() => {
    const name = `~${movedEl?.uid}`;
    const graph = pixi?.stage.getChildByName(name);

    if (graph) {
      graph.setTransform(movedEl.positionX, movedEl.positionY);
    }
  }, [pixi, movedEl]);
};
