import { Application } from 'pixi.js';
import { Dispatch, MutableRefObject, ReactNode, SetStateAction } from 'react';
import { NodeType } from '~/hooks';

interface Value {
  page?: string;
  element?: string;
}

export interface SelectionContextValue {
  pixiRef?: MutableRefObject<Application>;
  xRef?: MutableRefObject<HTMLInputElement>;
  yRef?: MutableRefObject<HTMLInputElement>;

  selecteds: Value;
  setSelecteds: Dispatch<SetStateAction<Value>>;
  onSelect?: (type: NodeType, uid?: string) => void;
}

export type SelectionValueHook = (
  onSelect?: SelectionContextValue['onSelect'],
) => SelectionContextValue;

export type SelectionContextHook = () => Value &
  Pick<SelectionContextValue, 'pixiRef' | 'xRef' | 'yRef'> & {
    onNodeSelect: SelectionContextValue['onSelect'];
  };

export type SelectionHighlightHook = (value: SelectionContextValue) => void;
export type GraphicsGearingHook = (value: SelectionContextValue) => void;

export interface SelectionProviderProps extends Pick<SelectionContextValue, 'onSelect'> {
  children: ReactNode;
}
