import ue from '@testing-library/user-event';
import { EventBoundary, FederatedPointerEvent, Graphics } from 'pixi.js';
import { act } from 'react-dom/test-utils';

import Index from './index.page';
import { fireEvent, render, screen } from '~/jest.utils';

describe('pages/index', () => {
  it('integration', async () => {
    const user = ue.setup();
    let graphs: Graphics[];

    const handleGraphicsChange = jest.fn((graphics: Graphics[]) => {
      graphs = graphics;
    });

    render(<Index onGraphicsChange={handleGraphicsChange} />);

    //* Add Page
    await act(() => user.click(screen.getByTestId('add-page')));
    expect(screen.getByTestId('page-item-0')).toBeInTheDocument();
    expect(screen.getByTestId('add-element')).toBeEnabled();

    //* Page Description
    const pageE = { target: { value: 'PAGE' } };

    await act(() => user.dblClick(screen.getByTestId('page-item-0')));
    fireEvent.blur(screen.getByTestId('desc-editor'), pageE);

    expect(screen.getByTestId('page-display-0')).toHaveTextContent(pageE.target.value);

    //* Page Selection
    expect(screen.getByTestId('add-element')).toBeDisabled();
    await act(() => user.click(screen.getByTestId('page-item-0')));

    //* Add Element
    await act(() => user.click(screen.getByTestId('add-element')));
    expect(screen.getByTestId('element-item-0')).toBeInTheDocument();
    expect(graphs.length).toBe(1);

    //* Graphics Selection
    act(() =>
      graphs[0].emit('pointertap', new FederatedPointerEvent(new EventBoundary(graphs[0]))),
    );

    expect(screen.getByTestId('positionX')).toBeInTheDocument();
    expect(screen.getByTestId('positionY')).toBeInTheDocument();
    expect(screen.getByTestId('opacity')).toBeInTheDocument();
    expect(screen.getByTestId('color')).toBeInTheDocument();

    //* Element Selection
    await act(() => user.click(screen.getByTestId('element-item-0')));
    expect(screen.queryAllByTestId('positionX').length).toBe(0);
    expect(screen.queryAllByTestId('positionY').length).toBe(0);
    expect(screen.queryAllByTestId('opacity').length).toBe(0);
    expect(screen.queryAllByTestId('color').length).toBe(0);

    await act(() => user.click(screen.getByTestId('element-item-0')));
    expect(screen.getByTestId('positionX')).toBeInTheDocument();
    expect(screen.getByTestId('positionY')).toBeInTheDocument();
    expect(screen.getByTestId('opacity')).toBeInTheDocument();
    expect(screen.getByTestId('color')).toBeInTheDocument();

    //* Edit Description
    const elementE = { target: { value: 'ELEMENT' } };

    await act(() => user.dblClick(screen.getByTestId('element-item-0')));
    fireEvent.blur(screen.getByTestId('desc-editor'), elementE);

    expect(screen.getByTestId('element-display-0')).toHaveTextContent(elementE.target.value);

    await act(() => user.click(screen.getByTestId('element-item-0')));

    //* Update Graphics by Editor
    const red = '#ff0000';
    const positionX = screen.getByTestId('positionX').querySelector('input');
    const positionY = screen.getByTestId('positionY').querySelector('input');
    const opacity = screen.getByTestId('opacity').querySelector('input');
    const color = screen.getByTestId('color').querySelector('input');

    fireEvent.change(opacity, { target: { name: 'opacity', value: '0.43' } });
    fireEvent.change(color, { target: { name: 'color', value: red } });
    await act(() => user.type(positionX, '200'));
    await act(() => user.type(positionY, '100'));

    expect(positionX).toHaveValue('200');
    expect(graphs[0].transform.position.x).toBe(200);
    expect(positionY).toHaveValue('100');
    expect(graphs[0].transform.position.y).toBe(100);
    expect(opacity).toHaveValue('0.43');
    expect(graphs[0].alpha).toBe(0.43);
    expect(color).toHaveValue(red);
    expect(graphs[0].fill.color).toBe(Number.parseInt(red.substring(1), 16));

    //* Update Graphics by DnD
    const canvas = screen.getByTestId('canvas');

    graphs[0].emit('pointerdown', new FederatedPointerEvent(new EventBoundary(graphs[0])));

    await act(() =>
      user.pointer([
        { target: canvas, keys: '[MouseLeft>]' },
        { target: canvas, coords: { x: 200, y: 200 } },
        { target: canvas, keys: '[/MouseLeft]' },
      ]),
    );

    const {
      transform: { position },
    } = graphs[0];

    expect(position.x).toBe(400);
    expect(position.y).toBe(300);

    //* Remove
    await act(() => user.click(screen.getByTestId('remove-element-0')));
    await act(() => user.click(screen.getByTestId('remove-page-0')));
    expect(screen.queryAllByTestId('element-item-0').length).toBe(0);
    expect(screen.queryAllByTestId('page-item-0').length).toBe(0);
  });
});
