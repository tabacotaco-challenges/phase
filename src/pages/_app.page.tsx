import CssBaseline from '@mui/material/CssBaseline';
import NoSsr from '@mui/material/NoSsr';
import type { AppProps } from 'next/app';
import { ThemeProvider } from '@mui/material/styles';

import * as themes from '~/themes';
import { MainContainer } from '~/styles';

export default function App({ Component, pageProps }: AppProps) {
  return (
    <NoSsr>
      <ThemeProvider theme={themes.dark}>
        <CssBaseline />

        <MainContainer maxWidth="lg" className="app" component="main">
          <Component {...pageProps} />
        </MainContainer>
      </ThemeProvider>
    </NoSsr>
  );
}
