# Phase Assignment **"Editor"**
This is a basic layout of the graphics editor.
The live demo is [here](https://tabacotaco-challenges.gitlab.io/phase/).

## UI
- **LeftPanel** with **Page** list section and **Element** list section.
- **Canvas**, where all **Elements** of the currently selected **Page** need to be displayed.
- **RightPanel** which displays properties of the currently selected **Element**.

## Features
- **Page** adding / switching / removing.
- **Element** adding / selection / removing.
- Update the element based on property changes in **RightPanel**.
- Drag-and-drop elements on the **Canvas**.
- Double-click to rename for elements / pages.
- Support nested element.
- Support drag-and-drop the nested group of elements.

## Dependencies
- Build By - [create-next-app](https://www.npmjs.com/package/create-next-app)
- Basic UI - [@mui](https://mui.com/)
- Graphics Tools - [pixi.js](https://pixijs.com/)
- Data State Management - [zustand](https://www.npmjs.com/package/zustand)
- Testing Framework - [jest](https://jestjs.io/)