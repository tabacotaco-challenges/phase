const basePath = process.env.NODE_ENV === 'production' ? '/phase' : '';

/** @type {import('next').NextConfig} */
const nextConfig = {
  basePath,
  reactStrictMode: true,
  pageExtensions: ['page.tsx'],
};

module.exports = nextConfig;
